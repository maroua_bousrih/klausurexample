package at.campus02.ase.books;

public class Book {
	
	private String autor;
	private String titel;
	private int seiten;

	public Book(String autor, String titel, int seiten) {
		this.autor=autor;
		this.titel=titel;
		this.seiten=seiten;
		
	}

	public String getAutor() {
		return "";
	}

	public String getTitel() {
		return "";
	}

	public int getSeiten() {
		return 0;
	}

	public boolean match(String search) {
		return false;
	}
	public String toString()
	{
		return String.format("[%s / %s / %d ]", autor,titel,seiten);
	}
}
